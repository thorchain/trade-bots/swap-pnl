requests==2.22.0
psycopg2==2.8.6
flask==1.1.2
influxdb-client==1.11.0
gunicorn==20.0.4
