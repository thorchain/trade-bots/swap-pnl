import os
from datetime import date, datetime

from flask import Flask, jsonify, request, abort
from flask.json import JSONEncoder

from psql import Database
from tendermint import Tendermint


class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


def toDate(dateString):
    return datetime.fromtimestamp(int(dateString))


app = Flask(__name__)
app.json_encoder = CustomJSONEncoder


@app.route("/ping")
def ping():
    return "pong"


@app.route("/address/<address>")
def get_address(address):
    pool = request.args.get("pool", default=None, type=str)
    start = request.args.get("start", default=None, type=toDate)
    end = request.args.get("end", default=None, type=toDate)

    dbm = Database()
    records = dbm.get_address(address, pool, start, end)

    return jsonify([record for record in records])


@app.route("/pools")
def get_pools():
    dbm = Database()
    records = dbm.get_pools()
    return jsonify([record["pool"] for record in records])


@app.route("/roi")
def get_roi():
    pool = request.args.get("pool", default=None, type=str)
    start = request.args.get("start", default=None, type=toDate)
    end = request.args.get("end", default=None, type=toDate)

    dbm = Database()
    records = dbm.get_roi(pool, start, end)

    return jsonify([record for record in records])


@app.route("/height")
def get_height():
    ip = os.environ.get("IP", None)
    tendermint = Tendermint(ip + ":27147")
    dbm = Database()

    current_height = dbm.get_last_block_height()
    latest_height = tendermint.get_height()
    return jsonify({"current_height": current_height, "latest_height": latest_height})


@app.route("/swap/<hash_id>")
def get_swap(hash_id):
    dbm = Database()
    swap = dbm.get_swap(hash_id)
    if swap is None:
        abort(404)
    else:
        return jsonify(swap)


@app.route("/swaps")
def list_swaps():
    offset = request.args.get("offset", default=0, type=int)
    limit = request.args.get("limit", default=50, type=int)
    if limit > 50:
        limit = 50
    address = request.args.get("address", default=None, type=str)
    pool = request.args.get("pool", default=None, type=str)
    start = request.args.get("start", default=None, type=toDate)
    end = request.args.get("end", default=None, type=toDate)

    dbm = Database()
    records = dbm.list_swaps(offset, limit, address, pool, start, end)

    return jsonify([record for record in records])


@app.route("/swaps/graph")
def swaps_graph():
    address = request.args.get("address", default=None, type=str)
    pool = request.args.get("pool", default=None, type=str)
    start = request.args.get("start", default=None, type=toDate)
    end = request.args.get("end", default=None, type=toDate)
    interval = request.args.get("end", default="1 day", type=str)

    dbm = Database()
    records = dbm.graph_swaps(address, pool, start, end, interval)

    return jsonify([record for record in records])


@app.route("/swaps/leaderboard")
def swaps_leaderboard():
    start = request.args.get("start", default=None, type=toDate)
    end = request.args.get("end", default=None, type=toDate)

    dbm = Database()
    records = dbm.get_leaderboard(start, end)

    return jsonify([record for record in records])
