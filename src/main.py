import logging
from datetime import datetime
import os
import sys
import signal
import time
import multiprocessing as mp
from queue import Queue

from api import app
from psql import Database
from coinpaprika import Coinpaprika
from tendermint import Tendermint
from binance import Binance
from thorchain import Thorchain

# Init logging
logging.basicConfig(
    format="%(asctime)s | %(levelname).4s | %(message)s",
    level=os.environ.get("LOGLEVEL", "INFO"),
)


def munch_swaps():
        ip = os.environ.get("IP", None)
        if ip is None:
            print("no IP address given")
            exit(1)

        binance = Binance("https://dex.binance.org/api/v1")
        paprika = Coinpaprika("https://api.coinpaprika.com/v1")
        tendermint = Tendermint(ip + ":27147")
        thorchain = Thorchain(ip + ":1317")

        genesis_time = tendermint.get_genesis_time()
        chain_age = datetime.now().timestamp() - genesis_time
        current_height = tendermint.get_height()
        block_time = chain_age / current_height
        logging.info(f"Block Time: {block_time}")

        dbm = Database()
        # we have a queue because not all swaps are complete. We need to wait for
        # the swap to complete before it can be added
        queue = Queue()

        if not dbm.table_exists("SWAPS"):
            logging.info("Creating tables...")
            dbm.create_tables()

        while True:
            try:
                current_height = tendermint.get_height()
                lastHeight = dbm.get_last_block_height()
                if lastHeight is not None:
                    lastHeight += 1
                if lastHeight is None or lastHeight <= 1:
                    lastHeight = 67405  # first block with a swap on chaosnet
                logging.info(f"Latest Height: {lastHeight}")

                # fetch events from THORChain
                for i in range(int(current_height / 1000)):
                    evts = tendermint.get_events(
                        lastHeight, min(lastHeight + 999, current_height), types=["swap"]
                    )
                    lastHeight = lastHeight + 999 + 1
                    for evt in evts:
                        evt["timestamp"] = datetime.utcfromtimestamp(
                            genesis_time + (evt["height"] * block_time)
                        )
                        queue.put(evt)

                    # process swaps...
                    for i in range(1, queue.qsize() + 1):
                        evt = queue.get()
                        ok = process_pnl(evt, thorchain, paprika, binance, dbm)
                        if not ok:
                            queue.put(evt)
                time.sleep(block_time)
            except Exception:
                logging.exception("fault munching swaps")


def process_pnl(evt, thorchain, paprika, binance, dbm):
    try:
        # fetch outbound hash
        tx = thorchain.get_tx(evt["id"])
        if tx["out_hashes"] is None or len(tx["out_hashes"]) == 0:
            return False
        out = tx["out_hashes"][0]
        tx = binance.get_tx(out)

        in_coin = {
            "denom": evt["coin"].split()[1],
            "amount": int(evt["coin"].split()[0]),
        }
        out_coin = tx["tx"]["value"]["msg"][0]["value"]["outputs"][0]["coins"][0]
        out_coin = {
            "denom": f"BNB.{out_coin['denom']}",  # TODO: assume BNB
            "amount": int(out_coin["amount"]),
        }

        ts = evt['timestamp']
        if int(ts.timestamp()) + 300 > int(datetime.now().timestamp()):
            ts = None
        in_price = paprika.get_price(in_coin["denom"], ts)
        out_price = paprika.get_price(out_coin["denom"], ts)
        spent = in_price * in_coin["amount"] / 1e8
        gained = out_price * out_coin["amount"] / 1e8

        evt["pnl"] = round(gained - spent, 2)
        logging.info(f"Swap ({evt['height']}): {out} {evt['from']} --> {evt['coin']} --> {evt['pool']} --> {evt['pnl']} PnL")
        dbm.insert_swap(evt)
    except Exception:
        logging.exception("failed to process pnl")
        return False
    return True


def main():
    logging.info("Starting to track swap PnL...")

    original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
    signal.signal(signal.SIGINT, original_sigint_handler)

    try:
        proc = mp.Process(target=munch_swaps)
        proc.start()
    except KeyboardInterrupt:
        print("Caught KeyboardInterrupt, terminating workers")
        sys.exit(3)

    port = int(os.environ.get("PORT", 5000))
    app.run(host="0.0.0.0", port=port)


if __name__ == "__main__":
    main()
