from datetime import datetime

from common import HttpClient


class Coinpaprika(HttpClient):
    """
    A local simple implementation of coinpaprika
    """

    def __init__(self, base_url):
        self.base_url = base_url
        self.ids = {
            # "BNB.BNB": "bnb-binance-coin",
            # "BNB.RUNE-B1A": "rune-thorchain",
            # "BNB.AVA-645": "ava-travala",
        }
        self.price_cache = {}

    def get_coin_id(self, asset):
        if asset in self.ids:
            return self.ids[asset]

        ticker = asset.split(".")[-1].split("-")[0]
        coins = self.get_coins()

        for coin in coins:
            if ticker == coin["symbol"]:
                self.ids[asset] = coin["id"]
                return coin["id"]

        return None

    def get_coins(self):
        return self.fetch("/coins")

    def _check_price_cache(self, coin_id, ts=None):
        # if ts is None:
        # ts = datetime.now().timestamp()
        # if coin_id in self.price_cache:
        # if int(ts) - int(self.price_cache[coin_id]['ts']) < 60:
        # return self.price_cache[coin_id]['price']
        return None

    def get_price(self, asset, ts=None):
        coin_id = self.get_coin_id(asset)
        if coin_id is None:
            return 0.0

        price = self._check_price_cache(coin_id, ts)
        if price is not None:
            return price

        if ts is None:
            resp = self.fetch("/tickers/" + coin_id)
            price = resp["quotes"]["USD"]["price"]
            self.price_cache[coin_id] = {
                "ts": int(datetime.now().timestamp()),
                "price": price,
            }
        else:
            params = {
                "start": int(ts.timestamp()),
                "end": int(ts.timestamp()) + 300,
                "quote": "usd",
            }
            resp = self.fetch("/tickers/" + coin_id + "/historical", params)
            price = resp[0]["price"]
            self.price_cache[coin_id] = {"ts": int(ts.timestamp()), "price": price}
        return self.price_cache[coin_id]["price"]
