import base64
import dateutil.parser

from common import HttpClient


def decode(msg):
    base64_bytes = msg.encode("ascii")
    message_bytes = base64.b64decode(base64_bytes)
    return message_bytes.decode("ascii")


class Tendermint(HttpClient):
    """
    A local simple implementation of tendermint of thorchain
    """

    def get_genesis_time(self):
        resp = self.fetch("/genesis")
        dt = dateutil.parser.parse(resp["result"]["genesis"]["genesis_time"])
        return dt.timestamp()

    def get_height(self):
        resp = self.fetch("/block")
        return int(resp["result"]["block"]["header"]["height"])

    def get_events(self, heightStart, heightEnd=None, types=None):
        if heightEnd is None:
            heightEnd = heightStart + 1

        data = []
        for i in range(heightStart, heightEnd):
            data.append(
                {
                    "jsonrpc": "2.0",
                    "id": i - heightStart + 1,
                    "method": "block_results",
                    "params": {"height": str(i)},
                }
            )

        resp = self.post("/", data)
        evts = []
        if not isinstance(resp, list):
            resp = [resp]
        for result in resp:
            todo = []
            if "result" not in result:
                continue
            result = result["result"]
            if (
                "begin_block_events" in result
                and result["begin_block_events"] is not None
            ):
                todo += result["begin_block_events"]
            if result["txs_results"] is not None:
                for data in result["txs_results"]:
                    todo += data["events"]
            if "end_block_events" in result and result["end_block_events"] is not None:
                todo += result["end_block_events"]
            for e in todo:
                evt = {
                    "type": e["type"],
                    "height": int(result["height"]),
                }
                for attr in e["attributes"]:
                    evt[decode(attr["key"])] = decode(attr["value"])
                if types is None or evt["type"] in types:
                    evts.append(evt)

        return evts
