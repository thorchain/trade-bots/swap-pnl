import os
import logging

import psycopg2
import psycopg2.extras
from psycopg2.extensions import AsIs


class Database:
    def __init__(
        self,
        host=os.getenv("PGHOST"),
        username=os.getenv("PGUSER"),
        password=os.getenv("PGPASSWORD"),
        dbname=os.getenv("PGDATABASE"),
        port=os.getenv("PGPORT", default="5432"),
        sslmode=os.getenv("PGSSLMODE", default="disable"),
    ):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.dbname = dbname
        self.sslmode = sslmode
        self.conn = None

    def connect(self):
        """Connect to a Postgres database."""
        if self.conn is None:
            try:
                self.conn = psycopg2.connect(
                    host=self.host,
                    user=self.username,
                    password=self.password,
                    port=self.port,
                    sslmode=self.sslmode,
                    dbname=self.dbname,
                )
            except psycopg2.DatabaseError as e:
                logging.error(e)
                raise e
            finally:
                logging.info("Connection opened successfully.")

    def select_rows(self, query):
        """Run a SQL query to select rows from table."""
        self.connect()
        records = []
        with self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
            cur.execute(query)
            rows = cur.fetchall()
            records = [dict(r) for r in rows]
            cur.close()
        return records

    def close(self):
        if self.conn:
            self.conn.commit()
            self.cursor.close()
            self.conn.close()

    def create_tables(self):
        """ create tables in the PostgreSQL database"""
        self.connect()
        commands = [
            """
            CREATE TABLE swaps (
                ts TIMESTAMPTZ NOT NULL,
                hash VARCHAR(64) NOT NULL,
                address VARCHAR(255) NOT NULL,
                pool VARCHAR(255) NOT NULL,
                height INTEGER NOT NULL,
                pnl FLOAT NOT NULL
            )
            """,
            """
            SELECT create_hypertable('swaps', 'ts')
            """,
            """
            CREATE INDEX indx_address ON swaps (address)
            """,
            """
            CREATE INDEX indx_pool ON swaps (pool)
            """,
            """
            CREATE INDEX indx_hash ON swaps (hash)
            """,
        ]
        cur = self.conn.cursor()
        for command in commands:
            logging.info(f"Creating: {command}")
            cur.execute(command)
        self.conn.commit()
        cur.close()

    def table_exists(self, table_str):
        table_str = table_str.lower()
        try:
            tables = self.select_rows(
                """
                SELECT * FROM pg_catalog.pg_tables
                WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';
                """
            )
            if len(tables) == 0:
                return False
            for table in tables:
                if table["tablename"] == table_str:
                    return True
        except psycopg2.Error as e:
            logging.error(e)
        return False

    def insert_swap(self, evt):
        swap = {
            "ts": evt["timestamp"],
            "hash": evt["id"],
            "pool": evt["pool"],
            "address": evt["from"],
            "height": evt["height"],
            "pnl": evt["pnl"],
        }

        if self.hash_exists(swap["hash"]):
            return

        columns = swap.keys()
        values = [swap[column] for column in columns]

        insert_statement = "insert into swaps (%s) values %s"

        cur = self.conn.cursor()
        result = cur.execute(insert_statement, (AsIs(",".join(columns)), tuple(values)))
        if result is not None:
            logging.error(result)
        self.conn.commit()
        cur.close()

    def insert(self, sql):
        logging.DEBUG(f"INSERT SQL: {sql}")
        cur = self.conn.cursor()
        cur.execute(sql)
        swap_id = cur.fetchone()[0]
        cur.close()
        return swap_id

    def hash_exists(self, hash_id):
        rows = self.select_rows(f"SELECT height FROM SWAPS WHERE hash = '{hash_id}'")
        return len(rows) > 0

    def get_last_block_height(self):
        rows = self.select_rows("SELECT MAX(height) FROM SWAPS")
        if len(rows) == 0:
            return 0
        else:
            return rows[0]["max"]

    def get_swap(self, hash_id):
        rows = self.select_rows(f"SELECT * FROM swaps WHERE hash = '{hash_id}'")
        if len(rows) == 0:
            return None
        else:
            return rows[0]

    def get_address(self, address, pool=None, start=None, end=None):
        query = "SELECT COUNT(pnl), SUM(pnl) as pnl FROM SWAPS"
        where_clauses = []
        where_clauses.append(f"address = '{address}'")
        if pool is not None:
            where_clauses.append(f"pool = '{pool}'")
        if start is not None:
            where_clauses.append(f"ts >= '{start}'")
        if end is not None:
            where_clauses.append(f"ts <= '{end}'")
        if len(where_clauses) > 0:
            query += " WHERE " + " and ".join(where_clauses)
        return self.select_rows(query)

    def get_leaderboard(self, start=None, end=None):
        query = "SELECT address, COUNT(pnl), SUM(pnl) as pnl FROM SWAPS"
        where_clauses = ["pnl > 0"]
        if start is not None:
            where_clauses.append(f"ts >= '{start}'")
        if end is not None:
            where_clauses.append(f"ts <= '{end}'")
        if len(where_clauses) > 0:
            query += " WHERE " + " and ".join(where_clauses)
        query += " GROUP BY address ORDER BY pnl DESC LIMIT 50"
        return self.select_rows(query)

    def get_pools(self):
        query = "SELECT DISTINCT(pool) FROM SWAPS"
        return self.select_rows(query)

    def get_roi(self, pool=None, start=None, end=None):
        query = "SELECT COUNT(pnl), SUM(pnl) as pnl FROM SWAPS"
        where_clauses = []
        if pool is not None:
            where_clauses.append(f"pool = '{pool}'")
        if start is not None:
            where_clauses.append(f"ts >= '{start}'")
        if end is not None:
            where_clauses.append(f"ts <= '{end}'")
        if len(where_clauses) > 0:
            query += " WHERE " + " and ".join(where_clauses)
        return self.select_rows(query)

    def graph_swaps(
        self, address=None, pool=None, start=None, end=None, interval="1 hour"
    ):
        query = f"""
        SELECT time_bucket('{interval}', ts) AS time,
        COUNT(pnl), SUM(pnl) as pnl FROM SWAPS
        """
        where_clauses = []
        if address is not None:
            where_clauses.append(f"address = '{address}'")
        if pool is not None:
            where_clauses.append(f"pool = '{pool}'")
        if start is not None:
            where_clauses.append(f"ts >= '{start}'")
        if end is not None:
            where_clauses.append(f"ts <= '{end}'")
        if len(where_clauses) > 0:
            query += " WHERE " + " and ".join(where_clauses)
        query += " GROUP BY time ORDER BY time DESC"
        return self.select_rows(query)

    def list_swaps(
        self, offset=0, limit=50, address=None, pool=None, start=None, end=None
    ):
        query = "SELECT * FROM SWAPS"
        offset_clause = f" OFFSET {offset} LIMIT {limit}"
        where_clauses = []
        if address is not None:
            where_clauses.append(f"address = '{address}'")
        if pool is not None:
            where_clauses.append(f"pool = '{pool}'")
        if start is not None:
            where_clauses.append(f"ts >= '{start}'")
        if end is not None:
            where_clauses.append(f"ts <= '{end}'")
        if len(where_clauses) > 0:
            query += " WHERE " + " and ".join(where_clauses)
        query += offset_clause
        return self.select_rows(query)
