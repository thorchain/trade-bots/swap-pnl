from common import HttpClient


class Thorchain(HttpClient):
    """
    A local simple implementation of thorchain chain
    """

    def get_tx(self, hash_id):
        return self.fetch("/thorchain/tx/" + hash_id)
