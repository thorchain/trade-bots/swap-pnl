from common import HttpClient


class Binance(HttpClient):
    """
    A local simple implementation of binance chain
    """

    def get_tx(self, hash_id):
        return self.fetch("/tx/" + hash_id + "?format=json")
